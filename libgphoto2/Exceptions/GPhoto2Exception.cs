﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  Exceptions/errors from libgphoto2 in a nice C# like interface.

using System;

namespace libgphoto2
{
    public static partial class GPhoto2
    {
        /// <summary>
        /// A general exception that comes from the gPhoto2 libraries.
        /// </summary>
        public class GPhoto2Exception : Exception
        {
            // Some functions return specific error codes
            private int errorCode;

            /// Get's the error code associated with the gPhoto2 exception
            public int ErrorCode
            {
                get { return errorCode; }
            }

            /// <summary>
            /// Creates a new gPhoto2 error.
            /// </summary>
            /// <param name="errorCode">Error code. from the call, default is assigned to the generic `GP_ERROR`</param>
            public GPhoto2Exception(int errorCode=GP_ERROR)
            {
                this.errorCode = errorCode;
            }

            /// <summary>
            /// Creates a new gPhoto2 error with a message attached.
            /// </summary>
            /// <param name="message">Message to send</param>
            /// <param name="errorCode">Error code. from the call, default is assigned to the generic `GP_ERROR`</param>
            public GPhoto2Exception(string message, int errorCode=GP_ERROR)
                : base(message)
            {
                this.errorCode = errorCode;
            }

            /// <summary>
            /// Creates a new gPhoto2 error with a message attached and an inner error.
            /// </summary>
            /// <param name="message">Message to send</param>
            /// <param name="inner">The exception that occured inside of this one</param>
            /// <param name="errorCode">Error code. from the call, default is assigned to the generic `GP_ERROR`</param>
            public GPhoto2Exception(string message, Exception inner, int errorCode=GP_ERROR)
                : base(message, inner)
            {
                this.errorCode = errorCode;
            }
        }
    }
}

