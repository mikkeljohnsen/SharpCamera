﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  A list of camera objects.

using System;
using System.Runtime.InteropServices;

namespace libgphoto2
{
    public static partial class GPhoto2
    {
        #region PInvoke Functions
        [DllImport("gphoto2")]
        private static extern int gp_list_new(ref IntPtr list);

        [DllImport("gphoto2")]
        private static extern int gp_list_free(IntPtr list);

        [DllImport("gphoto2")]
        private static extern int gp_list_count(IntPtr list);

        [DllImport("gphoto2")]
        private static extern int gp_list_get_name(IntPtr list, int index, ref IntPtr name);

        [DllImport("gphoto2")]
        private static extern int gp_list_get_value(IntPtr list, int index, ref IntPtr value);
        #endregion


        /// <summary>
        /// A List of Camera Objects
        /// </summary>
        public class CameraList : IDisposable {
            private bool disposed = false;
            public IntPtr handle;    // CameraList *


            #region Properties
            /// Counts the number of entires in the list
            public int Count
            {
                get { return gp_list_count(handle); }
            }
            #endregion


            public CameraList() {
                int ok = gp_list_new(ref handle);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("Not able to create a new CameraList", ok);
            }

            ~CameraList() {
                Dispose(false);
            }

            public void Dispose() {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            /// Does the actual cleanup work
            protected virtual void Dispose(bool disposing)
            {
                if (disposed)
                    return;

                // Free Managed
                if (disposing)
                {
                }

                // Free Unmanaged
                gp_list_free(handle);
                disposed = true;
            }

            /// <summary>
            /// Retrive the name of value (at the index).
            /// </summary>
            /// <returns>The name.</returns>
            /// <param name="index">Index of the object</param>
            public string GetName(int index)
            {
                IntPtr name = IntPtr.Zero;
                int ok = gp_list_get_name(handle, index, ref name);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("Error retriving name from list", ok);

                return Marshal.PtrToStringAnsi(name);
            }

            /// <summary>
            /// Return the value at an index.
            /// </summary>
            /// <returns>The value.</returns>
            /// <param name="index">index of the object</param>
            public string GetValue(int index)
            {
                IntPtr value = IntPtr.Zero;
                int ok = gp_list_get_value(handle, index, ref value);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("Error retriving value from list", ok);

                return Marshal.PtrToStringAnsi(value);
            }
        }
    }
}

