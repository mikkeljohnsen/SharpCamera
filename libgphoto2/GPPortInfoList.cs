﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  A list of data port connections.

using System;
using System.Runtime.InteropServices;

namespace libgphoto2
{
    public static partial class GPhoto2
    {
        #region PInvoke Functions
        [DllImport("gphoto2")]
        private static extern int gp_port_info_list_new(ref IntPtr list);

        [DllImport("gphoto2")]
        private static extern int gp_port_info_list_free(IntPtr list);

        [DllImport("gphoto2")]
        private static extern int gp_port_info_list_load(IntPtr list);

        [DllImport("gphoto2", CallingConvention=CallingConvention.Cdecl)]
        private static extern int gp_port_info_list_lookup_path(IntPtr list, [In] sbyte[] path);

        [DllImport("gphoto2")]
        private static extern int gp_port_info_list_get_info(IntPtr list, int n, out IntPtr info);
        #endregion


        /// <summary>
        /// A list of PortInfo objects
        /// </summary>
        public class GPPortInfoList : IDisposable
        {
            private bool disposed = false;
            public IntPtr handle;    // GPPortInfoList *


            public GPPortInfoList() {
                int ok = gp_port_info_list_new(ref handle);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("Not able to create a new GPPortInfoList", ok);
            }

            ~GPPortInfoList() {
                Dispose(false);
            }

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            /// Does the actual cleanup work
            protected virtual void Dispose(bool disposing)
            {
                if (disposed)
                    return;

                // Free Managed
                if (disposing)
                {
                }

                // Free Unmanaged
                gp_port_info_list_free(handle);
                disposed = true;
            }

            /// <summary>
            /// Searches for aviaiable Camear IO drivers
            /// </summary>
            public void Load()
            {
                int ok = gp_port_info_list_load(handle);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("Not able to find Camera IO drivers", ok);
            }

            /// <summary>
            /// Look for the index of a path in the list.
            /// </summary>
            /// <returns>The index of the path</returns>
            /// <param name="path">path to look for</param>
            public int LookupPath(string path)
            {
                int portNum = gp_port_info_list_lookup_path(handle, path.ToSByteArray());
                if (portNum == GP_ERROR_UNKNOWN_PORT)
                    throw new GPhoto2Exception("Unknown path/port provided", portNum);

                return portNum;
            }

            /// <summary>
            /// Get the Port Info at the index.
            /// </summary>
            /// <returns>info about the port.</returns>
            /// <param name="n">index of the object</param>
            public GPPortInfo GetInfo(int n)
            {
                // TODO this may need to be adjusted
                IntPtr info;
                int ok = gp_port_info_list_get_info(handle, n, out info);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("Error getting PortInfo from list", ok);

                return new GPPortInfo(info);
            }
        }
    }
}

