﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  Data Port information.

using System;
using System.Runtime.InteropServices;

namespace libgphoto2
{
    public static partial class GPhoto2
    {
        #region PInvoke Functions
        [DllImport("gphoto2")]
        private static extern int gp_port_info_new(out IntPtr info);

        [DllImport("gphoto2")]
        private static extern int gp_port_info_get_name(IntPtr info, out IntPtr name);

        [DllImport("gphoto2")]
        private static extern int gp_port_info_set_name(IntPtr info, IntPtr name);

        [DllImport("gphoto2")]
        private static extern int gp_port_info_get_path(IntPtr info, out IntPtr path);

        [DllImport("gphoto2")]
        private static extern int gp_port_info_set_path(IntPtr info, IntPtr name);

        [DllImport("gphoto2")]
        private static extern int gp_port_info_get_type(IntPtr info, out GPPortType type);

        [DllImport("gphoto2")]
        private static extern int gp_port_info_set_type(IntPtr info, GPPortType type);
        #endregion


        /// <summary>
        /// An object describing info about a port connection.
        /// </summary>
        public class GPPortInfo : IDisposable
        {
            private bool disposed = false;
            public IntPtr handle;


            /// <summary>
            /// Create a GPPortInfo object that's freshly allocated
            /// </summary>
            public GPPortInfo() {
                int ok = gp_port_info_new(out handle);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("Not able to create a new PortInfo object", ok);
            }

            /// <summary>
            /// Create a GPPortInfo object with a pre-allocated structure
            /// </summary>
            /// <param name="info">Info.</param>
            public GPPortInfo(IntPtr info)
            {
                this.handle = info;
            }

            ~GPPortInfo() {
                Dispose(false);
            }

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            /// Does the actual cleanup work
            protected virtual void Dispose(bool disposing)
            {
                if (disposed)
                    return;

                // Free Managed
                if (disposing)
                {
                }

                // Free Unmanaged
                disposed = true;
            }

            public string Name
            {
                get
                {
                    IntPtr name = IntPtr.Zero;
                    int ok = gp_port_info_get_name(handle, out name);
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("Error getting name from PortInfo", ok);

                    return Marshal.PtrToStringAnsi(name);
                }

                set
                {
                    IntPtr namePtr = Marshal.StringToHGlobalAnsi(value);
                    int ok = gp_port_info_set_name(handle, namePtr);
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("Error setting name for PortInfo", ok);

                    Marshal.FreeHGlobal(namePtr);
                }
            }

            public string Path
            {
                get
                {
                    IntPtr path = IntPtr.Zero;
                    int ok = gp_port_info_get_path(handle, out path);
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("Error getting path from PortInfo", ok);

                    return Marshal.PtrToStringAnsi(path);
                }

                set
                {
                    IntPtr pathPtr = Marshal.StringToHGlobalAnsi(value);
                    int ok = gp_port_info_set_path(handle, pathPtr);
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("Error setting path for PortInfo", ok);

                    Marshal.FreeHGlobal(pathPtr);
                }
            }

            public GPPortType Type
            {
                get
                {
                    GPPortType type;
                    int ok = gp_port_info_get_type(handle, out type);
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("Error getting type from PortInfo", ok);

                    return type;
                }

                set
                {
                    int ok = gp_port_info_set_type(handle, value);
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("Error setting type for PortInfo", ok);
                }
            }
        }
    }
}

