﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  General widget/control object on the Camera.

using System;
using System.Runtime.InteropServices;

namespace libgphoto2
{
    public static partial class GPhoto2
    {
        #region PInvoke Functions
        // This function is deprecated
        //        [DllImport("gphoto2")]
        //        private static extern int gp_widget_free(IntPtr widget);

        [DllImport("gphoto2")]
        private static extern int gp_widget_new(
            CameraWidgetType type,
            [MarshalAs(UnmanagedType.LPStr)] string label,
            out IntPtr widget
        );

        [DllImport("gphoto2")]
        private static extern int gp_widget_ref(IntPtr widget);

        [DllImport("gphoto2")]
        private static extern int gp_widget_unref(IntPtr widget);

        [DllImport("gphoto2")]
        private static extern int gp_widget_changed(IntPtr widget);

        [DllImport("gphoto2")]
        private static extern int gp_widget_set_changed(IntPtr widget, int changed);

        [DllImport("gphoto2")]
        private static extern int gp_widget_get_type(IntPtr widget, out CameraWidgetType type);

        [DllImport("gphoto2")]
        private static extern int gp_widget_get_value(IntPtr widget, out IntPtr value);
        // int gp_widget_get_value(CameraWidget *widget, void *value)
        [DllImport("gphoto2")]
        private static extern int gp_widget_set_value(IntPtr widget, IntPtr value);
        // int gp_widget_set_value(CameraWidget *widget, void *value)


        [DllImport("gphoto2", EntryPoint="gp_widget_get_value")]
        private static extern int gp_widget_get_value_int(IntPtr widget, out int value);

        [DllImport("gphoto2", EntryPoint="gp_widget_set_value")]
        private static extern int gp_widget_set_value_int(IntPtr widget, int value);


        // TODO some of these `IntPtr`s can actually be changed to proper strings (so we don't have to do the AllocHGlobal nonse
        [DllImport("gphoto2")]
        private static extern int gp_widget_get_label(IntPtr widget, out IntPtr label);

        [DllImport("gphoto2")]
        private static extern int gp_widget_get_name(IntPtr widget, out IntPtr name);

        [DllImport("gphoto2", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        private static extern int gp_widget_set_name(
            IntPtr widget,
            [MarshalAs(UnmanagedType.LPStr)] string name
        );

        [DllImport("gphoto2")]
        private static extern int gp_widget_get_info(IntPtr widget, out IntPtr info);

        [DllImport("gphoto2", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        private static extern int gp_widget_set_info(
            IntPtr widget,
            [MarshalAs(UnmanagedType.LPStr)] string info
        );

        [DllImport("gphoto2")]
        private static extern int gp_widget_get_id(IntPtr widget, out int id);

        [DllImport("gphoto2")]
        private static extern int gp_widget_get_readonly(IntPtr widget, out int ro);

        [DllImport("gphoto2")]
        private static extern int gp_widget_set_readonly(IntPtr widget, int ro);

        [DllImport("gphoto2")]
        private static extern int gp_widget_get_parent(IntPtr widget, out IntPtr parent);

        [DllImport("gphoto2")]
        private static extern int gp_widget_get_root(IntPtr widget, out IntPtr root);
        #endregion

        /// <summary>
        /// A widget on the Camera to control settings.
        /// </summary>
        public class CameraWidget : IDisposable
        {
            private bool disposed = false;
            private bool madeInLib = false;      // Did C# make the widget or was it made in the library?

            // TODO could this be set to `readonly` to make sure things are fudged w/ less?
            /// <summary>
            /// Handle for the underlying `CameraWidget *` pointer.  Only mess with this if you know what you're doing.
            /// </summary>
            public IntPtr handle;


            /// <summary>
            /// Create a Managed CameraWidget object from a previously instatiated object.
            /// </summary>
            internal CameraWidget(IntPtr widget)
            {
                madeInLib = true;
                handle = widget;
            }

            internal CameraWidget() { }

            /// <summary>
            /// Releases unmanaged resources and performs other cleanup operations before the
            /// <see cref="libgphoto2.GPhoto2.CameraWidget"/> is reclaimed by garbage collection.
            /// </summary>
            ~CameraWidget()
            {
                Dispose(false);
            }

            /// <summary>
            /// Creates a new CameraWidget object from a pointer handle, but will create the respective subclass
            /// of the widget's type.
            ///
            /// For example, if a CameraWidget's type is found to be a `CameraWidgetType.GP_WIDGET_RADIO`, then
            /// this will return a `RadioCameraWidget` object.
            ///
            /// It's not recommended you use this function unless you konw what you're doing.
            /// </summary>
            /// <returns>A CameraWidget subclass (but as a CameraWidget)</returns>
            /// <param name="widgetPtr">pointer/handle to the CameraWidget</param>
            public static CameraWidget NewFromType(IntPtr widgetPtr)
            {
                // Resolve the type
                CameraWidgetType type;
                int ok = gp_widget_get_type(widgetPtr, out type);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("Error determining the widget type", ok);

                // Make the specific widget object
                switch (type)
                {
                    case CameraWidgetType.GP_WIDGET_WINDOW:
                        return new WindowCameraWidget(widgetPtr);
                    case CameraWidgetType.GP_WIDGET_SECTION:
                        return new SectionCameraWidget(widgetPtr);
                    case CameraWidgetType.GP_WIDGET_TEXT:
                        return new TextCameraWidget(widgetPtr);
                    case CameraWidgetType.GP_WIDGET_RANGE:
                        return new RangeCameraWidget(widgetPtr);
                    case CameraWidgetType.GP_WIDGET_TOGGLE:
                        return new ToggleCameraWidget(widgetPtr);
                    case CameraWidgetType.GP_WIDGET_RADIO:
                        return new RadioCameraWidget(widgetPtr);
                    case CameraWidgetType.GP_WIDGET_MENU:
                        return new MenuCameraWidget(widgetPtr);
                    case CameraWidgetType.GP_WIDGET_BUTTON:
                        return new ButtonCameraWidget(widgetPtr);
                    case CameraWidgetType.GP_WIDGET_DATE:
                        return new DateCameraWidget(widgetPtr);

                    default:
                        // This should never, EVER be hit.
                        return new CameraWidget(widgetPtr);
                }
            }

            /// <summary>
            /// Releases all resource used by the <see cref="libgphoto2.GPhoto2.CameraWidget"/> object.
            /// </summary>
            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            /// Does the actual cleanup work
            protected virtual void Dispose(bool disposing)
            {
                if (disposed)
                    return;

                // Free Managed resources
                if (disposing)
                {
                }

                // Free unmanged resources
                if (!madeInLib)
                    gp_widget_unref(handle);

                disposed = true;
            }

            /// <summary>
            /// Tells if the widget has been changed.  Or tell the widget that is has been changed.
            ///
            /// In addition, it resets the changed flag.
            /// </summary>
            /// <value><c>true</c> if changed; otherwise, <c>false</c>.</value>
            public bool Changed
            {
                get
                {
                    int ok = gp_widget_changed(handle);
                    if (ok < GP_OK)
                        throw new GPhoto2Exception("Error checking if widget was changed", ok);

                    return (ok == 1);
                }

                set
                {
                    int ok = gp_widget_set_changed(handle, value ? 1 : 0);
                    if (ok < GP_OK)
                        throw new GPhoto2Exception("Error telling the widget it changed", ok);
                }
            }

            /// <summary>
            /// Get the type of the widget (e.g. Radio, Window, Date, etc.)
            /// </summary>
            /// <value>The type.</value>
            public CameraWidgetType Type
            {
                get
                {
                    CameraWidgetType type;
                    int ok = gp_widget_get_type(handle, out type);
                    if (ok != GP_OK)
                         throw new GPhoto2Exception("Error retriving widget type", ok);

                    return type;
                }
            }

            /// <summary>
            /// Name of the widget.
            /// </summary>
            /// <value>The name.</value>
            public string Name
            {
                get
                {
                    IntPtr namePtr;
                    int ok = gp_widget_get_name(handle, out namePtr);
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("Not able to get widget name", ok);

                    return Marshal.PtrToStringAnsi(namePtr);
                }

                set
                {
                    int ok = gp_widget_set_name(handle, value);
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("not able to set widget name", ok);
                }
            }

            /// <summary>
            /// A little information about the widget.
            /// </summary>
            /// <value>The info.</value>
            public string Info
            {
                get
                {
                    IntPtr infoPtr;
                    int ok = gp_widget_get_info(handle, out infoPtr);
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("Not able to get widget info", ok);

                    return Marshal.PtrToStringAnsi(infoPtr);
                }

                set
                {
                    int ok = gp_widget_set_info(handle, value);
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("not able to set widget info", ok);
                }
            }

            /// <summary>
            /// The label of the Widget.
            /// </summary>
            /// <value>The label.</value>
            public string Label
            {
                get
                {
                    IntPtr labelPtr;
                    int ok = gp_widget_get_label(handle, out labelPtr);
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("Not able to get widget label", ok);

                    return Marshal.PtrToStringAnsi(labelPtr);
                }
            }

            /// <summary>
            /// Unique Id for the widget.
            /// </summary>
            /// <value>The identifier.</value>
            public int Id
            {
                get
                {
                    int id;
                    int ok = gp_widget_get_id(handle, out id);
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("Not able to get widget id", ok);

                    return id;
                }
            }

            /// <summary>
            /// See if the widget cannot be changed.
            /// </summary>
            /// <value><c>true</c> if readonly; otherwise, <c>false</c>.</value>
            public bool Readonly
            {
                get
                {
                    int ro;
                    int ok = gp_widget_get_readonly(handle, out ro);
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("not able to get widget readonly flag", ok);

                    return (ro == 1);
                }

                set
                {
                    int ok = gp_widget_set_readonly(handle, value ? 1 : 0);
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("not able to set widget readonly flag", ok);
                }
            }

            /// <summary>
            /// Get the parent widget, could be null if there is no parent.
            /// </summary>
            /// <value>The parent widget, or null</value>
            public CameraWidget Parent
            {
                get
                {
                    IntPtr parent = IntPtr.Zero;
                    int ok = gp_widget_get_parent(handle, out parent);
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("Not able to get widget parent", ok);

                    // Is there actually one there?
                    if (parent != IntPtr.Zero)
                        return NewFromType(parent);
                    else
                        return null;
                }
            }


            /// <summary>
            /// Get the root widget from the CameraWidget tree.
            /// </summary>
            /// <value>A CameraWidget</value>
            public CameraWidget Root
            {
                get
                {
                    IntPtr root = IntPtr.Zero;
                    int ok = gp_widget_get_root(handle, out root);
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("Not able to get root widget", ok);

                    return NewFromType(root);
                }
            }
        }
    }
}

