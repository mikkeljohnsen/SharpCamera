﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  A CameraWidget sublcass for Button types.
using System;

namespace libgphoto2
{
    public static partial class GPhoto2
    {
        /// <summary>
        /// A Button widget.
        /// </summary>
        public class ButtonCameraWidget : CameraWidget
        {
            /// <summary>
            /// Create a widget that can be pressed, from a previously instatied object.
            /// </summary>
            /// <param name="widget">handle to a Button widget.</param>
            public ButtonCameraWidget(IntPtr widget)
                : base(widget)
            {
                if (Type != CameraWidgetType.GP_WIDGET_BUTTON)
                    throw new ArgumentException("Supplied handle is not a Button widget");
            }

            /// <summary>
            /// Create a brand new button.
            /// </summary>
            /// <param name="label">Label to give the widget</param>
            public ButtonCameraWidget(string label)
            {
                int ok = gp_widget_new(CameraWidgetType.GP_WIDGET_BUTTON, label, out handle);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("error creating new Button widget", ok);
            }
        }
    }
}