﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  A CameraWidget subclass for Dates.
using System;

namespace libgphoto2
{
    public static partial class GPhoto2
    {
        /// <summary>
        /// A Date widget.
        /// </summary>
        public class DateCameraWidget : CameraWidget
        {
            /// <summary>
            /// Create a widget for dates and time, from a previouly instatiated object.
            /// </summary>
            /// <param name="widget">handle to a Date widget.</param>
            public DateCameraWidget(IntPtr widget)
                : base(widget)
            {
                if (Type != CameraWidgetType.GP_WIDGET_DATE)
                    throw new ArgumentException("Supplied handle is not a Date widget");
            }

            /// <summary>
            /// Create a branch new Date widget.
            /// </summary>
            /// <param name="label">Label to give to the widget.</param>
            public DateCameraWidget(string label)
            {
                int ok = gp_widget_new(CameraWidgetType.GP_WIDGET_DATE, label, out handle);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("error creating new Date widget", ok);
            }

            /// <summary>
            /// Get or set the date value.
            ///
            /// At the moment, this type is an integer, but later on, it will be changed a C# DateTime object
            /// </summary>
            /// <value>DateTime </value>
            public DateTime Value
            {
                get
                {
                    int date;
                    int ok = gp_widget_get_value_int(handle, out date);
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("error retriving date value", ok);

                    return Utils.UnixTimestampToDateTime((uint)date);
                }

                set
                {
                    int ok = gp_widget_set_value_int(handle, (int)Utils.DateTimeToUnixTimestamp(value));
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("error setting date value", ok);
                }
            }
        }
    }
}

