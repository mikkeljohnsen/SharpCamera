﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  A CameraWidget for toggle buttons
using System;

namespace libgphoto2
{
    public static partial class GPhoto2
    {
        /// <summary>
        /// A Toggle widget (think check box)
        /// </summary>
        public class ToggleCameraWidget : CameraWidget
        {
            /// <summary>
            /// Create a widget that can toggle on/off.
            /// </summary>
            /// <param name="widget">handle to a Toggle widget.</param>
            public ToggleCameraWidget(IntPtr widget)
                : base(widget)
            {
                if (Type != CameraWidgetType.GP_WIDGET_TOGGLE)
                    throw new ArgumentException("Supplied handle is not a Toggle widget");
            }

            /// <summary>
            /// Create a branch new Toggle widget.
            /// </summary>
            /// <param name="label">Label to give to the widget</param>
            public ToggleCameraWidget(string label)
            {
                int ok = gp_widget_new(CameraWidgetType.GP_WIDGET_TOGGLE, label, out handle);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("error creating new Toggle widget", ok);
            }

            /// <summary>
            /// See if the Toggle is on or off.
            /// </summary>
            /// <value><c>true</c> if toggle is on; <c>false</c>. if it is not.</value>
            public bool Value
            {
                get
                {
                    int value;
                    int ok = gp_widget_get_value_int(handle, out value);
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("not able to get toggle", ok);

                    return (value == 1);
                }

                set
                {
                    int ok = gp_widget_set_value_int(handle, value ? 1 : 0);
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("not able to set toggle", ok);
                }
            }
        }
    }
}

