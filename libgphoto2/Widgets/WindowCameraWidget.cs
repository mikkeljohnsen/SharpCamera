﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  A CameraWidget for Window types.  (they have children)

using System;

namespace libgphoto2
{
    public static partial class GPhoto2
    {
        /// <summary>
        /// This is a top level configuration widget.
        ///
        /// It possibly contains multiple SectionCameraWidgets
        /// </summary>
        public class WindowCameraWidget : ContainerCameraWidget
        {
            /// <summary>
            /// Create the Window widget object, from a previously instantiated object.
            /// </summary>
            /// <param name="widget">handle to the Widget, must be a Window type</param>
            public WindowCameraWidget(IntPtr widget)
                : base(widget)
            {
                if (Type != CameraWidgetType.GP_WIDGET_WINDOW)
                    throw new ArgumentException("supplied handle is not for a Window widget");
            }

            /// <summary>
            /// Create a brand new Window widget.
            /// </summary>
            /// <param name="label">Label to give to the widget</param>
            public WindowCameraWidget(string label)
            {
                int ok = gp_widget_new(CameraWidgetType.GP_WIDGET_WINDOW, label, out handle);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("error creating new Window widget", ok);
            }
        }
    }

}

