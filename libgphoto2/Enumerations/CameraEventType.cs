// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  Camera event types.

using System;

namespace libgphoto2
{
    public static partial class GPhoto2
    {
        public enum CameraEventType
        {
            GP_EVENT_UNKNOWN,
            GP_EVENT_TIMEOUT,
            GP_EVENT_FILE_ADDED,
            GP_EVENT_FOLDER_ADDED,
            GP_EVENT_CAPTURE_COMPLETE,
            GP_EVENT_FILE_CHANGED
        }
    }
}
