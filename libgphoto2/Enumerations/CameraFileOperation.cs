﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  Camera file operations

using System;

namespace libgphoto2
{
    public static partial class GPhoto2
    {
        public enum CameraFileOperation
        {
            GP_FILE_OPERATION_NONE    = 0,          // No special file operations, just download.
            GP_FILE_OPERATION_DELETE  = 1 << 1,     // Deletion of files is possible.
            GP_FILE_OPERATION_PREVIEW = 1 << 3,     // Previewing viewfinder content is possible.
            GP_FILE_OPERATION_RAW     = 1 << 4,     // Raw retrieval is possible (used by non-JPEG cameras)
            GP_FILE_OPERATION_AUDIO   = 1 << 5,     // Audio retrieval is possible.
            GP_FILE_OPERATION_EXIF    = 1 << 6      // EXIF retrieval is possible.
        }
    }
}

