﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  Camera widget enumeration.

using System;

namespace libgphoto2
{
    public static partial class GPhoto2
    {
        public enum CameraWidgetType
        {
            GP_WIDGET_WINDOW,
            GP_WIDGET_SECTION,
            GP_WIDGET_TEXT,
            GP_WIDGET_RANGE,
            GP_WIDGET_TOGGLE,
            GP_WIDGET_RADIO,
            GP_WIDGET_MENU,
            GP_WIDGET_BUTTON,
            GP_WIDGET_DATE
        }
    }

}

