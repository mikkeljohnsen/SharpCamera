.. contents::

===========
SharpCamera
===========

* ``libgphoto2`` |libgphoto2_badge|_
* ``SharpCamera`` |SharpCamera_badge|_

SharpCamera is a C# interface into using tethered cameras (via ``libgphoto2``
under the hood).  This project also includes C# friendly bindings to libgphoto2,
but it's recommended you don't use them unless you know what you're doing, they
are incomplete as it is anyways, but more will be added in later.

SharpCamera and the libgphoto2 bindings are licensed under the terms of the GNU
LGPLv3.  See the file ``LICENSE-LGPLv3.txt`` for more details.

I don't have much documentation up at the moment, so feel free to ask me
something on the issue tracker.  Same goes for if you'd like a feature added.

Since this software is still pre-1.0, it's API is subject to radical change at
any time.  The libgphoto2 will try to very close to the original C API, but in
a more C# friendly format, where as SharpCamera is meant to be more higher level
and easier to use.


How to Use & Dependencies
=========================

* You need the native C libgphoto2 DLL (or shared object) on the machine where
  you want to use this library

  * This should also require ``libusb`` too
  * On Linux you can get this (probably) from your package manager
  * On OS X this can be grabbed using Homebrew


Documentation
-------------

Not all of the libgphoto2 bindings are properly documented, but SharpCamera
should be.  You can generate HTML documentation if you have Doxygen installed
by running the `doxygen` command in the root of this project.  It's also
recommed you have GraphViz installed as well to generate some nice graphs.

* In the future, I'd like to use GitLab CI & GitLab Pages to auto generate and
  publish the docs.  I could use some help with this.


Running on Windows
------------------

This project is activley developed on a Linux system (and tested on an
OS X machine).  Theoretically SharpCamera and the libgphoto2 bindings should
work on Windows, but I have yet to build & install the libgphoto2 native C
library on the Microsoft platform.

I actually appreciate some help with this at the moment.  If you want to, here
would be the best place to start:
https://github.com/gphoto/libgphoto2/issues/97#issuecomment-353842267

It would be nice in the end to be able to put the native DLL into the NuGet
package as well, so I could use assistance with that too.


Sample Apps
===========

There are a few sample applications showing you how to use the libgphoto2
bindings and SharpCamera.  They are all under the MIT license.  See the file
``LICENSE-MIT.txt`` for more details.


ShowWidgetInfo
--------------

Uses SharpCamera to connect to the first available camera on the system (if
found) and will then use the lower level libgphoto2 bindings to show all of
the widgets available on the camera (in a tree format).  This is a console app.


SimpleCapture
-------------

Uses SharpCamera to scan for the first camera available, establish a connection,
and then take a photo.  This is a console app.


LivePreviewGtk
--------------

This goes a little further and gets a live preview image from a Camera and
displays it in a in a GTK3 window (using `GtkSharp <https://github.com/GtkSharp/GtkSharp>`_).
This is (definately) a GUI app.



.. |libgphoto2_badge| image:: https://badge.fury.io/nu/libgphoto2.svg
.. _libgphoto2_badge: https://badge.fury.io/nu/libgphoto2

.. |SharpCamera_badge| image:: https://badge.fury.io/nu/SharpCamera.svg
.. _SharpCamera_badge: https://badge.fury.io/nu/SharpCamera
